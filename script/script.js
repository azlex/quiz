'use strict';

document.addEventListener('DOMContentLoaded', function() {

    const btnOpenModal = document.getElementById('btnOpenModal');
    const modalBlock = document.getElementById('modalBlock');
    const closeModal = document.getElementById('closeModal');
    const questionTitle = document.getElementById('question');
    const formAnswers = document.getElementById('formAnswers');
    const burgerBtn = document.getElementById('burger');
    const modalWrap = document.querySelector('.modal');
    const modalDialog = document.querySelector('.modal-dialog');
    const nextButton = document.getElementById('next');
    const prevButton = document.getElementById('prev');
    const sendButton = document.getElementById('send');
    const modalTitle = document.querySelector('.modal-title');

    let clientWidth = document.documentElement.clientWidth;
    let count = -100;

    const questions = [
        {
            question: "Какого цвета бургер?",
            answers: [
                {
                    title: 'Стандарт',
                    url: './image/burger.png'
                },
                {
                    title: 'Черный',
                    url: './image/burgerBlack.png'
                }
            ],
            type: 'radio'
        },
        {
            question: "Из какого мяса котлета?",
            answers: [
                {
                    title: 'Курица',
                    url: './image/chickenMeat.png'
                },
                {
                    title: 'Говядина',
                    url: './image/beefMeat.png'
                },
                {
                    title: 'Свинина',
                    url: './image/porkMeat.png'
                }
            ],
            type: 'radio'
        },
        {
            question: "Дополнительные ингредиенты?",
            answers: [
                {
                    title: 'Помидор',
                    url: './image/tomato.png'
                },
                {
                    title: 'Огурец',
                    url: './image/cucumber.png'
                },
                {
                    title: 'Салат',
                    url: './image/salad.png'
                },
                {
                    title: 'Лук',
                    url: './image/onion.png'
                }
            ],
            type: 'checkbox'
        },
        {
            question: "Добавить соус?",
            answers: [
                {
                    title: 'Чесночный',
                    url: './image/sauce1.png'
                },
                {
                    title: 'Томатный',
                    url: './image/sauce2.png'
                },
                {
                    title: 'Горчичный',
                    url: './image/sauce3.png'
                }
            ],
            type: 'radio'
        }
    ];

    modalDialog.style.top = count + "%";
    
    if(clientWidth < 767) {
        burgerBtn.style.display = 'flex';
    } else {
        burgerBtn.style.display = 'none';
    }

    window.addEventListener('resize', () => {
        clientWidth = document.documentElement.clientWidth;

        if(clientWidth < 767) {
            burgerBtn.style.display = 'flex';
        } else {
            burgerBtn.style.display = 'none';
        }
    })

    const animateModal = () => {
        modalDialog.style.top = count + "%";
        count += 3;
        
        if(count < 0) {
            requestAnimationFrame(animateModal);            
        } else {
            count = -100;
        }
    }

    burgerBtn.addEventListener('click', () => {
        if(burgerBtn.classList.contains('active')) {
            burgerBtn.classList.remove('active');
        } else {
            burgerBtn.classList.add('active');
        }
        modalBlock.classList.add('d-block');
        playTest();
    })

    btnOpenModal.addEventListener('click', () => {
        requestAnimationFrame(animateModal);
        modalBlock.classList.add('d-block');
        playTest();
    });

    closeModal.addEventListener('click', () => modalBlock.classList.remove('d-block'));

    modalWrap.addEventListener('click', () => burgerBtn.classList.remove('active'));

    document.addEventListener('click', (event) => {
        if(
            !event.target.closest('.modal-dialog') && 
            !event.target.closest('.openModalButton') && 
            !event.target.closest('.burger')) {
            modalBlock.classList.remove('d-block');
        }
    })

    const playTest = () => {   

        const finalAnswers = [];
        const obj = [];

        let numberQuestion = 0;

        const renderAnswers = (index) => {
           questions[index].answers.forEach((answer, idx) => {
                const answerItem = document.createElement('div');
                answerItem.classList.add('answers-item', 'd-flex', 'justify-content-center')
                answerItem.innerHTML = `
                    <input type="${questions[index].type}" id="answerItem${index}${idx}" name="answer" class="d-none" value="${answer.title}">
                    <label for="answerItem${index}${idx}" class="d-flex flex-column justify-content-between">
                    <img class="answerImg" src="${answer.url}" alt="burger">
                    <span>${answer.title}</span>
                    </label>
                `;
                formAnswers.appendChild(answerItem);
            });
        }

        const renderQuestions = (indexQuestion) => {
            formAnswers.innerHTML = '';

            if(numberQuestion >= 0 && numberQuestion <= questions.length - 1) {
                questionTitle.textContent = `${questions[indexQuestion].question}`;
                renderAnswers(indexQuestion);    
                nextButton.classList.remove('d-none');
                prevButton.classList.remove('d-none');
                sendButton.classList.add('d-none');
            }

            if(numberQuestion === 0) {
                prevButton.classList.add('d-none');
            }

            if(numberQuestion === questions.length) {
                nextButton.classList.add('d-none');
                prevButton.classList.add('d-none');
                sendButton.classList.remove('d-none');
                questionTitle.textContent = '';
                modalTitle.textContent = '';
                formAnswers.innerHTML = `
                    <div class="form-group">
                        <label for="numberPhone">Enter your phone</label>
                        <input type="text" class="form-control" id="numberPhone"/>
                    </div>
                `;
                const numberPhone = document.getElementById('numberPhone');
                numberPhone.addEventListener('input', (event) => {
                    event.target.value = event.target.value.replace(/[^0-9+-]/, '');
                });
            } 

            if(numberQuestion === questions.length + 1){
                formAnswers.textContent = 'Many thanks for test!';
                sendButton.classList.add('d-none');

                for(let key in obj){
                    let newObj = [];
                    newObj[key] = obj[key];
                    finalAnswers.push(newObj);
                }

                setTimeout(() => {
                    modalBlock.classList.remove('d-block');
                }, 2000);
            }
        };

        renderQuestions(numberQuestion);  

        const checkAnswer = () => {
            const inputs = [...formAnswers.elements].filter((input) => input.checked || input.id === 'numberPhone');

            inputs.forEach((input, index) => {
                if(numberQuestion >= 0 && numberQuestion <= questions.length - 1){
                    obj[`${index}_${questions[numberQuestion].question}`] = input.value;
                }

                if(numberQuestion === questions.length) {
                    obj['Phone number'] = input.value;
                }
                
            });            
        };

        nextButton.onclick = () => {
            checkAnswer();
            numberQuestion++;
            renderQuestions(numberQuestion);
        };

        prevButton.onclick = () => {
            numberQuestion--;
            renderQuestions(numberQuestion);
        };

        sendButton.onclick = () => {
            checkAnswer();
            numberQuestion++;
            renderQuestions(numberQuestion);
            console.log(finalAnswers);
        }
    }
})


